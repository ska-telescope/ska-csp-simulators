# pylint: disable=redefined-outer-name
# -*- coding: utf-8 -*-
"""
Some simple unit tests of the LowCbfAllocatorSimulator device, exercising the device from
another host using a DeviceProxy.
"""
import logging

import pytest
import tango
from ska_control_model import AdminMode, HealthState

from ska_csp_simulators.DevFactory import DevFactory
from ska_csp_simulators.low.low_cbf_allocator_simulator import (
    LowCbfAllocatorSimulator,
)

module_logger = logging.getLogger(__name__)


@pytest.fixture()
def devices_to_load():
    return (
        {
            "class": LowCbfAllocatorSimulator,
            "devices": [
                {
                    "name": "sim-low-cbf/allocator/0",
                },
            ],
        },
    )


@pytest.fixture
def allocator_device(tango_context):
    """Create DeviceProxy for tests"""
    logging.info("%s", tango_context)
    dev_factory = DevFactory()

    return dev_factory.get_device("sim-low-cbf/allocator/0")


@pytest.fixture(autouse=True)
def allocator_device_online(allocator_device, change_event_callbacks):
    for attribute in [
        "state",
        "adminMode",
        "healthState",
        "longRunningCommandProgress",
        "longRunningCommandStatus",
        "longRunningCommandResult",
    ]:
        allocator_device.subscribe_event(
            attribute,
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks[attribute],
        )
        allocator_device.subscribe_event(
            attribute,
            tango.EventType.CHANGE_EVENT,
            print,
        )
    change_event_callbacks.assert_change_event("state", tango.DevState.DISABLE)
    change_event_callbacks.assert_change_event("adminMode", AdminMode.OFFLINE)
    change_event_callbacks.assert_change_event(
        "healthState", HealthState.UNKNOWN
    )
    change_event_callbacks.assert_change_event(
        "longRunningCommandProgress", ()
    )
    change_event_callbacks.assert_change_event("longRunningCommandStatus", ())
    change_event_callbacks.assert_change_event(
        "longRunningCommandResult", ("", "")
    )
    allocator_device.adminmode = 0
    change_event_callbacks.assert_change_event("adminMode", AdminMode.ONLINE)
    change_event_callbacks.assert_change_event("state", tango.DevState.ON)


def test_allocator_device_is_alive(allocator_device):
    """Sanity check: test device on remote host is responsive"""
    try:
        allocator_device.ping()
    except tango.ConnectionFailed:
        pytest.fail("Could not contact the allocator device")


def test_allocator_register_fsps(allocator_device):
    """Verify that fsps are registered correctly."""
    allocator_device.ForceRegisterFsps(
        '{"serial": ["XFL1RCFEG244"], "fqdn": "low-cbf/processor/0.0.0"}'
    )
    allocator_device.ForceRegisterFsps(
        '{"serial": ["XFL1E35JVJTQ"], "fqdn": "low-cbf/processor/0.0.1"}'
    )

    assert (
        allocator_device.fsps
        == "{'fsp_01': ['XFL1RCFEG244'], 'fsp_02': ['XFL1E35JVJTQ']}"
    )
    assert (
        allocator_device.procdevfqdn
        == "{'XFL1RCFEG244': 'low-cbf/processor/0.0.0', 'XFL1E35JVJTQ': 'low-cbf/processor/0.0.1'}"
    )
