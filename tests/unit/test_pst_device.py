# pylint: disable=redefined-outer-name
# -*- coding: utf-8 -*-
"""
Some simple unit tests of the PstBeamSimulatorDevice device,
exercising the device from another host using a DeviceProxy.
"""
import json
import logging

import pytest
import tango
from ska_control_model import (
    AdminMode,
    HealthState,
    ObsMode,
    ObsState,
    ResultCode,
)
from ska_control_model.pst_processing_mode import PstProcessingMode

from ska_csp_simulators.common.pst_beam_simulator import PstBeamSimulatorDevice
from ska_csp_simulators.DevFactory import DevFactory

module_logger = logging.getLogger(__name__)


@pytest.fixture()
def devices_to_load():
    return (
        {
            "class": PstBeamSimulatorDevice,
            "devices": [
                {
                    "name": "sim-pst/beam/01",
                },
            ],
        },
    )


@pytest.fixture
def pst_device(tango_context):
    """Create DeviceProxy for tests"""
    logging.info("%s", tango_context)
    dev_factory = DevFactory()

    return dev_factory.get_device("sim-pst/beam/01")


@pytest.fixture(autouse=True)
def pst_device_online(pst_device, change_event_callbacks):
    for attribute in [
        "state",
        "adminMode",
        "healthState",
        "obsState",
        "obsMode",
        "pstProcessingMode",
        "channelBlockConfiguration",
        "longRunningCommandProgress",
        "longRunningCommandStatus",
        "longRunningCommandResult",
    ]:
        pst_device.subscribe_event(
            attribute,
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks[attribute],
        )
    change_event_callbacks.assert_change_event("state", tango.DevState.DISABLE)
    change_event_callbacks.assert_change_event("adminMode", AdminMode.OFFLINE)
    change_event_callbacks.assert_change_event("healthState", HealthState.OK)
    change_event_callbacks.assert_change_event("obsState", ObsState.IDLE)
    change_event_callbacks.assert_change_event("obsMode", ObsMode.IDLE)
    change_event_callbacks.assert_change_event(
        "pstProcessingMode", PstProcessingMode.IDLE.name
    )
    change_event_callbacks.assert_change_event(
        "channelBlockConfiguration", "{}"
    )
    change_event_callbacks.assert_change_event(
        "longRunningCommandProgress", ()
    )
    change_event_callbacks.assert_change_event("longRunningCommandStatus", ())
    change_event_callbacks.assert_change_event(
        "longRunningCommandResult", ("", "")
    )
    pst_device.adminmode = 0
    change_event_callbacks.assert_change_event("adminMode", AdminMode.ONLINE)
    change_event_callbacks.assert_change_event("state", tango.DevState.OFF)


def test_pst_beam_attributes_configured_for_events(
    pst_device, change_event_callbacks
):
    """Test that the PST Beam attributes are configured to push events"""
    for attribute in [
        "dataReceived",
        "dataReceiveRate",
        "dataDropped",
        "dataDropRate",
    ]:
        pst_device.subscribe_event(
            attribute,
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks[attribute],
        )
    change_event_callbacks.assert_change_event("dataReceived", 0)
    change_event_callbacks.assert_change_event("dataReceiveRate", 0.0)
    change_event_callbacks.assert_change_event("dataDropped", 0)
    change_event_callbacks.assert_change_event("dataDropRate", 0.0)


def test_pst_beam_configure_and_gotoidle(pst_device, change_event_callbacks):
    """Test configure request on PST Beam"""
    # load the cahnnel block configuration from a data file. This  file
    # is the same used by the PST simulator during beam configuration.
    f = open(
        "./tests/test_data/pst_channel_block_config.json",
        "r",
        encoding="utf-8",
    )
    config_json = """
        {
        "interface": "https://schema.skao.int/ska-pst-configure/2.5",
        "common": {
            "config_id": "sbi-mvp01-20240111-voltage_recorder",
            "subarray_id": 1,
            "eb_id": "eb-x321-20240111-10012",
            "frequency_band": "low"
        },
        "pst": {
            "scan": {
                "activation_time": "2024-01-11T23:11:17Z",
                "bits_per_sample": 32,
                "timing_beam_id": "1",
                "num_of_polarizations": 2,
                "udp_nsamp": 32,
                "wt_nsamp": 32,
                "udp_nchan": 24,
                "num_frequency_channels": 432,
                "centre_frequency": 100000000.0,
                "total_bandwidth": 1562500.0,
                "observation_mode": "VOLTAGE_RECORDER",
                "observer_id": "jdoe",
                "project_id": "project1",
                "pointing_id": "pointing1",
                "source": "J1921+2153",
                "itrf": [5109360.133, 2006852.586, -3238948.127],
                "receiver_id": "receiver3",
                "feed_polarization": "LIN",
                "feed_handedness": 1,
                "feed_angle": 1.234,
                "feed_tracking_mode": "FA",
                "feed_position_angle": 10.0,
                "oversampling_ratio": [4, 3],
                "coordinates": {
                    "equinox": 2000.0,
                    "ra": "19:21:44.815",
                    "dec": "21:53:02.400"
                },
                "max_scan_length": 20000.0,
                "subint_duration": 30.0,
                "receptors": ["SKA001", "SKA036"],
                "receptor_weights": [0.4, 0.6],
                "num_channelization_stages": 2,
                "channelization_stages": [{
                    "num_filter_taps": 1,
                    "filter_coefficients": [1.0],
                    "num_frequency_channels": 1024,
                    "oversampling_ratio": [32, 27]
                }, {
                    "num_filter_taps": 1,
                    "filter_coefficients": [1.0],
                    "num_frequency_channels": 256,
                    "oversampling_ratio": [4, 3]
                }]
            }
        }
    }
    """
    channel_config = f.read().replace("\n", "")
    pst_device.forcestate(tango.DevState.ON)
    change_event_callbacks.assert_change_event("state", tango.DevState.ON)
    pst_device.forceobsstate(ObsState.READY)
    change_event_callbacks.assert_change_event("obsState", ObsState.READY)
    [[result_code], [command_id]] = pst_device.Configure(config_json)
    assert result_code == ResultCode.QUEUED
    change_event_callbacks.assert_change_event(
        "obsState", ObsState.CONFIGURING
    )
    change_event_callbacks.assert_change_event(
        "longRunningCommandStatus", (command_id, "STAGING")
    )
    change_event_callbacks.assert_change_event(
        "longRunningCommandStatus", (command_id, "IN_PROGRESS")
    )
    change_event_callbacks.assert_change_event(
        "longRunningCommandResult",
        (command_id, '[0, "Task completed successfully"]'),
    )
    change_event_callbacks.assert_change_event(
        "longRunningCommandStatus", (command_id, "COMPLETED")
    )

    change_event_callbacks.assert_change_event(
        "channelBlockConfiguration", channel_config
    )
    change_event_callbacks.assert_change_event("obsState", ObsState.READY)
    change_event_callbacks.assert_change_event(
        "pstProcessingMode", PstProcessingMode.VOLTAGE_RECORDER.name
    )
    assert json.loads(pst_device.channelBlockConfiguration) == json.loads(
        channel_config
    )
    [[result_code], [gti_id]] = pst_device.GoToIdle()
    assert result_code == ResultCode.QUEUED
    change_event_callbacks.assert_change_event(
        "longRunningCommandStatus",
        (command_id, "COMPLETED", gti_id, "STAGING"),
    )
    change_event_callbacks.assert_change_event(
        "longRunningCommandStatus",
        (command_id, "COMPLETED", gti_id, "IN_PROGRESS"),
    )
    change_event_callbacks.assert_change_event(
        "longRunningCommandResult",
        (gti_id, '[0, "Task completed successfully"]'),
    )
    change_event_callbacks.assert_change_event(
        "longRunningCommandStatus",
        (command_id, "COMPLETED", gti_id, "COMPLETED"),
    )

    change_event_callbacks.assert_change_event("obsState", ObsState.IDLE)
    change_event_callbacks.assert_change_event(
        "pstProcessingMode", PstProcessingMode.IDLE.name
    )
    change_event_callbacks.assert_change_event(
        "channelBlockConfiguration", "{}"
    )
    assert json.loads(pst_device.channelBlockConfiguration) == {}


@pytest.mark.parametrize(
    "init_obs_state",
    [
        ObsState.ABORTED,
        ObsState.FAULT,
    ],
)
def test_pst_beam_restart(pst_device, init_obs_state, change_event_callbacks):
    """Test obsreset request on PST Beam"""
    pst_device.forcestate(tango.DevState.ON)
    change_event_callbacks.assert_change_event("state", tango.DevState.ON)
    pst_device.forceobsstate(init_obs_state)
    change_event_callbacks.assert_change_event("obsState", init_obs_state)
    [[result_code], [command_id]] = pst_device.ObsReset()
    assert result_code == ResultCode.QUEUED
    change_event_callbacks.assert_change_event("obsState", ObsState.RESETTING)
    change_event_callbacks.assert_change_event(
        "longRunningCommandStatus", (command_id, "STAGING")
    )
    change_event_callbacks.assert_change_event(
        "longRunningCommandStatus", (command_id, "IN_PROGRESS")
    )
    change_event_callbacks.assert_change_event(
        "longRunningCommandResult",
        (command_id, '[0, "ObsReset completed"]'),
    )
    change_event_callbacks.assert_change_event(
        "longRunningCommandStatus", (command_id, "COMPLETED")
    )
    change_event_callbacks.assert_change_event("obsState", ObsState.IDLE)


def test_beam_configure_not_allowed_in_wrong_state(pst_device):
    """Test beaam configure not allowed in wrong state"""
    assert pst_device.state() == tango.DevState.OFF
    with pytest.raises(tango.DevFailed):
        pst_device.ConfigureScan('{"subarray_id":1}')


@pytest.mark.parametrize(
    "device_init_obs_state",
    [
        ObsState.EMPTY,
        ObsState.RESTARTING,
        ObsState.CONFIGURING,
        ObsState.ABORTED,
        ObsState.FAULT,
        ObsState.ABORTING,
        ObsState.RESOURCING,
        ObsState.SCANNING,
    ],
)
def test_beam_configure_not_allowed_in_wrong_obs_state(
    pst_device, device_init_obs_state, change_event_callbacks
):
    """Test beam configure not allowed in wrong observing state"""
    pst_device.forcestate(tango.DevState.ON)
    change_event_callbacks.assert_change_event("state", tango.DevState.ON)
    pst_device.forceobsstate(device_init_obs_state)
    change_event_callbacks.assert_change_event(
        "obsState", device_init_obs_state
    )
    with pytest.raises(tango.DevFailed):
        pst_device.ConfigureScan('{"subarray_id":1}')


def test_pst_beam_scan(pst_device, change_event_callbacks):
    """Test scan request on PST Beam"""
    # load the cahnnel block configuration from a data file. This  file
    # is the same used by the PST simulator during beam configuration.
    pst_device.forcestate(tango.DevState.ON)
    change_event_callbacks.assert_change_event("state", tango.DevState.ON)
    pst_device.forceobsstate(ObsState.READY)
    change_event_callbacks.assert_change_event("obsState", ObsState.READY)
    [[result_code], [command_id]] = pst_device.Scan('{"subarray_id":1}')
    assert result_code == ResultCode.QUEUED
    change_event_callbacks.assert_change_event("obsState", ObsState.SCANNING)
    change_event_callbacks.assert_change_event(
        "longRunningCommandStatus", (command_id, "STAGING")
    )
    change_event_callbacks.assert_change_event(
        "longRunningCommandStatus", (command_id, "IN_PROGRESS")
    )
