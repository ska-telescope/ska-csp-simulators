# pylint: disable=redefined-outer-name
# -*- coding: utf-8 -*-
"""
Some simple unit tests of the SubarraySimulatorDevice device, exercising the device from
another host using a DeviceProxy.
"""
import logging
import time

import pytest
import tango
from ska_control_model import AdminMode, HealthState, ObsState, ResultCode

from ska_csp_simulators.DevFactory import DevFactory
from ska_csp_simulators.low.low_cbf_processor_simulator import (
    LowCbfProcessorSimulator,
)

module_logger = logging.getLogger(__name__)


@pytest.fixture()
def devices_to_load():
    return (
        {
            "class": LowCbfProcessorSimulator,
            "devices": [
                {
                    "name": "sim-low-cbf/processor/0.0.0",
                },
            ],
        },
    )


@pytest.fixture
def processor_device(tango_context):
    """Create DeviceProxy for tests"""
    logging.info("%s", tango_context)
    dev_factory = DevFactory()

    return dev_factory.get_device("sim-low-cbf/processor/0.0.0")


@pytest.fixture(autouse=True)
def processor_device_online(processor_device, change_event_callbacks):
    for attribute in [
        "state",
        "adminMode",
        "healthState",
        "obsState",
        "longRunningCommandProgress",
        "longRunningCommandStatus",
        "longRunningCommandResult",
    ]:
        processor_device.subscribe_event(
            attribute,
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks[attribute],
        )
    change_event_callbacks.assert_change_event("state", tango.DevState.DISABLE)
    change_event_callbacks.assert_change_event("adminMode", AdminMode.OFFLINE)
    change_event_callbacks.assert_change_event("healthState", HealthState.OK)
    change_event_callbacks.assert_change_event("obsState", ObsState.IDLE)
    change_event_callbacks.assert_change_event(
        "longRunningCommandProgress", ()
    )
    change_event_callbacks.assert_change_event("longRunningCommandStatus", ())
    change_event_callbacks.assert_change_event(
        "longRunningCommandResult", ("", "")
    )
    processor_device.adminmode = 0
    change_event_callbacks.assert_change_event("adminMode", AdminMode.ONLINE)
    change_event_callbacks.assert_change_event("state", tango.DevState.ON)


def test_obs_faulty(processor_device):
    assert not processor_device.obsFaulty
    processor_device.obsFaulty = True
    assert processor_device.obsFaulty


def test_processor_device_is_alive(processor_device):
    """Sanity check: test device on remote host is responsive"""
    try:
        processor_device.ping()
    except tango.ConnectionFailed:
        pytest.fail("Could not contact the base device")


def test_processor_configure(processor_device, change_event_callbacks):
    """Verify that stats_mode and subarrayIds are updated correctly."""

    change_event_callbacks.assert_change_event(
        "healthState", HealthState.UNKNOWN
    )
    processor_device.forceobsstate(ObsState.READY)
    change_event_callbacks.assert_change_event(
        "obsState", ObsState.READY, lookahead=2
    )
    [[result_code], [command_id]] = processor_device.Configure(
        '{"ready": "true", "firmware": "pst0.0.1", "subarray_ids": "1"}'
    )
    assert result_code == ResultCode.QUEUED
    change_event_callbacks.assert_change_event(
        "obsState", ObsState.CONFIGURING, lookahead=2
    )
    change_event_callbacks.assert_change_event(
        "longRunningCommandStatus", (command_id, "STAGING"), lookahead=3
    )
    change_event_callbacks.assert_change_event(
        "longRunningCommandStatus", (command_id, "IN_PROGRESS"), lookahead=2
    )
    change_event_callbacks.assert_change_event(
        "longRunningCommandResult",
        (command_id, '[0, "Task completed successfully"]'),
    )
    change_event_callbacks.assert_change_event(
        "longRunningCommandStatus", (command_id, "COMPLETED"), lookahead=2
    )
    change_event_callbacks.assert_change_event(
        "obsState", ObsState.READY, lookahead=2
    )
    assert (
        processor_device.stats_mode
        == '{"ready": "true", "firmware": "pst0.0.1"}'
    )
    assert processor_device.subarrayIds == [1]
    [[result_code], [command_id]] = processor_device.Scan('{"subarray_id":1}')
    assert result_code == ResultCode.QUEUED
    change_event_callbacks.assert_change_event("obsState", ObsState.SCANNING)
    # this wait is neceessary because updates are happening only every 20s
    time.sleep(22)
    assert "total_pkts_in" in processor_device.stats_io
    assert "total_pkts_out" in processor_device.stats_io
