# -*- coding: utf-8 -*-
#
# This file is part of the CSP Simulators project
#
# GPL v3
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

""" Simulator for the PST beam sub-system
"""

from __future__ import annotations

import json
import threading
import time

from ska_control_model import ObsState, ResultCode, TaskStatus
from ska_control_model.pst_processing_mode import PstProcessingMode
from tango import DebugIt, DevState
from tango.server import attribute, command, run

from ska_csp_simulators.common.obs_simulator import ObsSimulatorDevice
from ska_csp_simulators.utilities.data_generator import generate_random_number

__all__ = ["PstBeamSimulatorDevice", "main"]


DevVarLongStringArrayType = tuple[list[ResultCode], list[str]]
# pylint: disable=logging-fstring-interpolation


SCAN_TIME = 5


class RepeatFunction(threading.Timer):
    def run(self):
        while not self.finished.wait(self.interval):
            self.function(*self.args, **self.kwargs)


class PstBeamSimulatorDevice(ObsSimulatorDevice):
    """
    Base simulator device
    """

    def init_device(self):
        """Initialises the attributes and properties of the device."""
        super().init_device()
        self._channel_block_configuration = {}
        self._processing_mode = PstProcessingMode.IDLE
        self._data_receive_rate = 0
        self._data_received = 0
        self._data_drop_rate = 0
        self._data_dropped = 0
        self.timer = RepeatFunction(1, self._update_random_attributes)
        self.set_change_event("channelBlockConfiguration", True, False)
        self.set_change_event("pstProcessingMode", True, False)
        self.set_change_event("dataReceived", True, False)
        self.set_change_event("dataReceiveRate", True, False)
        self.set_change_event("dataDropped", True, False)
        self.set_change_event("dataDropRate", True, False)

    # ---------------
    # General methods
    # ---------------
    def update_channel_block(self, attr_value: str):
        # load the JSON string into a dict
        self._channel_block_configuration = json.loads(attr_value)
        # update the attribute using the string value
        self.push_change_event("channelBlockConfiguration", attr_value)

    def update_processing_mode(
        self: PstBeamSimulatorDevice, value: PstProcessingMode
    ):
        if self._processing_mode != value:
            self._processing_mode = value
            self.push_change_event("pstProcessingMode", value.name)

    def _update_random_attributes(self: PstBeamSimulatorDevice):

        if self._obs_state == ObsState.SCANNING:
            self.logger.debug(f"Update random attributes at {time.asctime()}")
            self._data_receive_rate = generate_random_number(
                data_type="float", range_start=0, range_end=200
            )
            self._data_received = generate_random_number(
                data_type="int", range_start=0, range_end=10000000000
            )
            self._data_dropped = generate_random_number(
                data_type="int", range_start=0, range_end=10000000000
            )
            self._data_drop_rate = generate_random_number(
                data_type="int", range_start=-1, range_end=200
            )
        else:
            self._data_received = 0
            self._data_receive_rate = 0.0
            self._data_dropped = 0
            self._data_drop_rate = 0.0
        self.push_change_event("dataReceiveRate", self._data_receive_rate)
        self.push_change_event("dataReceived", self._data_received)
        self.push_change_event("dataDropped", self._data_dropped)
        self.push_change_event("dataDropRate", self._data_drop_rate)

    # ----------
    # Attributes
    # ----------
    @attribute(
        dtype=str,
        doc="The channel block configuration based on scan configuration.",
    )
    def channelBlockConfiguration(self: PstBeamSimulatorDevice) -> str:
        # convert the dict information to a string
        return json.dumps(self._channel_block_configuration)

    # TODO: this attribute will be renamed into Processing mode
    @attribute(
        dtype=str,
        label="PST Processing Mode",
        doc="Report processing mode of the beam",
    )
    def pstProcessingMode(self: PstBeamSimulatorDevice) -> str:
        """
        Get the current processing mode that PST is configured for.

        If the BEAM is not in a scan configured state, this will return "IDLE".
        """
        return self._processing_mode.name

    @attribute(
        dtype=int,
        unit="Bytes",
        standard_unit="Bytes",
        display_unit="B",
        doc="Total number of bytes received from the CBF in the current scan",
    )
    def dataReceived(self: PstBeamSimulatorDevice) -> int:
        """
        Get the total amount of data received from CBF interface for current scan.

        :returns: total amount of data received from CBF interface for current scan in Bytes
        :rtype: int
        """
        return self._data_received

    @attribute(
        dtype=float,
        unit="Gigabits per second",
        standard_unit="Gigabits per second",
        display_unit="Gb/s",
        max_value=200,
        min_value=0,
        doc="Current data receive rate from the CBF interface",
    )
    def dataReceiveRate(self: PstBeamSimulatorDevice) -> float:
        """
        Get the current data receive rate from the CBF interface.

        :returns: current data receive rate from the CBF interface in Gb/s.
        :rtype: float
        """
        return self._data_receive_rate

    @attribute(
        dtype=float,
        label="Drop Rate",
        unit="Bytes per second",
        standard_unit="Bytes per second",
        display_unit="B/s",
        max_value=200,
        min_value=-1,
        doc="Current rate of CBF ingest data being dropped or lost by the receiving process",
    )
    def dataDropRate(self: PstBeamSimulatorDevice) -> float:
        """
        Get the current rate of CBF ingest data being dropped or lost by the receiving proces.

        :returns: current rate of CBF ingest data being dropped or lost in Bytes/s.
        :rtype: float
        """
        return self._data_drop_rate

    @attribute(
        dtype=int,
        label="Dropped",
        unit="Bytes",
        standard_unit="Bytes",
        display_unit="B",
        doc="Total number of bytes dropped in the current scan",
    )
    def dataDropped(self: PstBeamSimulatorDevice) -> int:
        """
        Get the total number of bytes dropped in the current scan.

        :returns: total number of bytes dropped in the current scan.
        :rtype: int
        """
        return self._data_dropped

    # --------
    # Commands
    # --------

    @command(dtype_in=str)
    @DebugIt()
    def ForceProcessingMode(self: ObsSimulatorDevice, value: str) -> None:
        """
        Force the subarray observing mode to the desired value
        """
        self.logger.info(
            f"Force observing state from {PstProcessingMode(self._processing_mode).name} "
            f"to {PstProcessingMode(value).name}"
        )
        self.update_processing_mode(PstProcessingMode(value))

    @command(dtype_in=str, dtype_out="DevVarLongStringArray")
    @DebugIt()
    def Configure(self, argin):
        """
        Subarray configure resources
        """
        self.check_raise_exception()

        def _configure_completed():
            self.logger.info("Command Configure completed on device")
            if self._obs_faulty:
                return self.update_obs_state(ObsState.FAULT)
            if self._faulty_in_command:
                self.update_obs_state(ObsState.IDLE)
            if (
                not self._abort_event.is_set()
                and not self._obs_faulty
                and not self._faulty_in_command
            ):
                with open(
                    "./tests/test_data/pst_channel_block_config.json",
                    "r",
                    encoding="utf-8",
                ) as f:
                    channel_config = f.read().replace("\n", "")
                    self.update_channel_block(channel_config)
                self.update_obs_state(ObsState.READY)
                self.update_processing_mode(processing_mode)
            self._update_random_attributes()

        self.logger.info(f"Configure invoked at time {time.asctime()}")
        argin_dict = json.loads(argin)
        # retrive the processing mode for the pst beam
        processing_mode_label = argin_dict["pst"]["scan"]["observation_mode"]
        processing_mode = PstProcessingMode[processing_mode_label]
        self.update_obs_state(ObsState.CONFIGURING)
        result_code, msg = self.do(
            "configure", completed=_configure_completed, argin=argin_dict
        )
        return ([result_code], [msg])

    def is_ConfigureScan_allowed(self: PstBeamSimulatorDevice) -> bool:
        """
        Return whether `ConfigureScan` may be called in the current device state.

        :raises ValueError: command not permitted in observation state

        :return: whether the command may be called in the current device
            state
        """
        if (
            self._obs_state not in [ObsState.IDLE, ObsState.READY]
            or self.get_state() != DevState.ON
        ):
            raise ValueError(
                "ConfigureScan command not permitted in observation state "
                f"{ObsState(self._obs_state).name} or state {self.get_state()}"
            )
        return True

    @command(dtype_in=str, dtype_out="DevVarLongStringArray")
    @DebugIt()
    def ConfigureScan(self, argin):
        """
        Subarray configure resources
        """
        return self.Configure(argin)

    @command(dtype_out="DevVarLongStringArray")
    @DebugIt()
    def GoToIdle(self):
        self.check_raise_exception()

        def _end_completed():
            self.logger.info("Command GotoIdle completed on device}")
            self.update_obs_state(ObsState.IDLE)
            self.update_processing_mode(PstProcessingMode.IDLE)
            self.update_channel_block("{}")

        self.logger.info(f"GoToIdle invoked at time {time.asctime()}")
        result_code, msg = self.do("end", completed=_end_completed)
        return ([result_code], [msg])

    @command(dtype_in=str, dtype_out="DevVarLongStringArray")
    @DebugIt()
    def Scan(self, argin):
        def _scan_completed():
            self.logger.info("Scan completed on device")
            if self._obs_faulty:
                self.update_obs_state(ObsState.FAULT)
            if self._faulty_in_command:
                self.update_obs_state(ObsState.READY)
            self._time_to_complete = 0.4
            self.logger.info(
                f"Reset timeToComplete to {self._time_to_complete} sec"
            )
            self._update_random_attributes()

        self.logger.info(f"Scan invoked at time {time.asctime()}")
        self._time_to_complete = SCAN_TIME
        self.check_raise_exception()
        argin_dict = json.loads(argin)
        self.update_obs_state(ObsState.SCANNING)
        self.timer.start()
        result_code, msg = self.do(
            "scan", completed=_scan_completed, argin=argin_dict
        )
        return ([result_code], [msg])

    @command(dtype_out="DevVarLongStringArray")
    @DebugIt()
    def EndScan(self):
        self.check_raise_exception()

        def _endscan():
            self.logger.info("Event endscan started")
            self._command_tracker.update_command_info(
                command_id, status=TaskStatus.IN_PROGRESS
            )
            time.sleep(0.2)
            msg = "Endscan Task completed successfully"
            self._command_tracker.update_command_info(
                command_id,
                status=TaskStatus.COMPLETED,
                result=[ResultCode.OK, msg],
            )
            self.update_obs_state(ObsState.READY)
            self.timer.cancel()
            self._update_random_attributes()

        self.logger.info(f"EndScan invoked at time {time.asctime()}")
        command_id = self._command_tracker.new_command("endscan")
        self._endscan_event.set()
        threading.Thread(target=_endscan).start()
        result_code, _ = ResultCode.STARTED, "EndScan invoked"
        return ([result_code], [command_id])

    @command(dtype_out="DevVarLongStringArray")
    @DebugIt()
    def Abort(self):
        def _abort():
            self._command_tracker.update_command_info(
                command_id, status=TaskStatus.IN_PROGRESS
            )
            time.sleep(0.2)
            self._command_tracker.update_command_info(
                command_id, status=TaskStatus.COMPLETED
            )

        obs_state_ori = self._obs_state

        self.update_obs_state(ObsState.ABORTING)
        command_id = self._command_tracker.new_command("abort")
        self.logger.info("Invoking Abort")

        result_code, _ = ResultCode.STARTED, "Abort invoked"
        if obs_state_ori in [ObsState.IDLE, ObsState.READY]:
            self.update_obs_state(ObsState.ABORTED)
            self._obs_faulty = False
            result_code = ResultCode.OK
        else:
            self.timer.cancel()
            self._update_random_attributes()
            self._abort_event.set()
            threading.Thread(target=_abort).start()
        return ([result_code], [command_id])


# ----------
# Run server
# ----------


def main(args=None, **kwargs):
    """Main function of the device module."""
    return run((PstBeamSimulatorDevice,), args=args, **kwargs)


if __name__ == "__main__":
    main()
