# -*- coding: utf-8 -*-
#
# This file is part of the CSP Simulators project
#
# GPL v3
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

""" Simulator for the processor of a CSP sub-system CBF
"""
from __future__ import annotations

import json
import time

from ska_control_model import HealthState, ResultCode
from tango import DebugIt, DevState, DevVarLongStringArray
from tango.server import attribute, command, run

from ska_csp_simulators.common.base_simulator_device import BaseSimulatorDevice

__all__ = ["LowCbfAllocatorSimulator", "main"]

DevVarLongStringArrayType = tuple[list[ResultCode], list[str]]
# pylint: disable=logging-fstring-interpolation


class LowCbfAllocatorSimulator(BaseSimulatorDevice):
    """
    CBF Allocator simulator device
    """

    def init_device(self):
        """Initialises the attributes and properties of the device."""
        super().init_device()
        self._health_state = HealthState.UNKNOWN
        self._fsps = {}
        self._procdevfqdn = {}

        self.set_change_event("fsps", True, False)
        self.set_change_event("procDevFqdn", True, False)

    # ---------------
    # General methods
    # ---------------
    def set_communication(
        self: LowCbfAllocatorSimulator,
        end_state,
        end_health,
        connecting: bool,
    ):
        """
        Override the behavior of the method to set different
        values fot the State and healthState of the device.
        """

        super().set_communication(DevState.ON, HealthState.UNKNOWN, connecting)

    def update_fsps(self: LowCbfAllocatorSimulator):
        """Reset fsps"""
        if self._fsps:
            self._fsps = {}
            self.push_change_event("fsps", self._fsps)

    def update_procdevfqdn(self: LowCbfAllocatorSimulator):
        """Reset procdevfqdn"""
        if self._procdevfqdn:
            self._procdevfqdn = {}
            self.push_change_event("procDevFqdn", self.procdevfqdn)

    # ----------
    # Attributes
    # ----------

    @attribute(
        dtype="DevString",
        label="Serial numbers of fsps",
    )
    def fsps(self: LowCbfAllocatorSimulator):
        """Return the pssBeams attribute."""
        return str(self._fsps)

    @fsps.write  # type: ignore[no-redef]
    def fsps(self: LowCbfAllocatorSimulator, value) -> None:
        self._fsps = value

    @attribute(
        dtype="DevString",
        label="Processor device fqdns",
    )
    def procdevfqdn(self: LowCbfAllocatorSimulator):
        """Return the procdevfqdns attribute."""
        return str(self._procdevfqdn)

    @procdevfqdn.write  # type: ignore[no-redef]
    def procdevfqdn(self: LowCbfAllocatorSimulator, value) -> None:
        self._procdevfqdn = value

    # --------
    # Commands
    # --------

    @command(
        dtype_out="DevVarLongStringArray",
    )
    @DebugIt()
    def On(self: LowCbfAllocatorSimulator) -> DevVarLongStringArray:
        message = "Ignored ON, controller does not have hardware"
        self.logger.error(message)
        return [[ResultCode.REJECTED], [message]]

    @command(
        dtype_out="DevVarLongStringArray",
    )
    @DebugIt()
    def Off(self: LowCbfAllocatorSimulator) -> DevVarLongStringArray:
        message = "Ignored OFF, controller does not have hardware"
        self.logger.error(message)
        return [[ResultCode.REJECTED], [message]]

    @command(
        dtype_out="DevVarLongStringArray",
    )
    @DebugIt()
    def Standby(self: LowCbfAllocatorSimulator) -> DevVarLongStringArray:
        message = "Ignored STANDBY, controller does not have hardware"
        self.logger.error(message)
        return [[ResultCode.REJECTED], [message]]

    @command(
        dtype_out="DevVarLongStringArray",
    )
    @DebugIt()
    def Reset(self: LowCbfAllocatorSimulator) -> DevVarLongStringArray:
        message = "Ignored RESET, controller does not have hardware"
        self.logger.error(message)
        return [[ResultCode.REJECTED], [message]]

    @command(dtype_in=str, dtype_out=None)
    @DebugIt()
    def ForceRegisterFsps(self, argin: str):
        """Register processor to fsp

        :param argin: 'DevString'
        Serial numbers and FQDN to register, JSON string
        e.g.
        {
           "serial": ["A123", "B456", "C789"],
           "fqdn":  "low-cbf/processor/0.1.0"
        }
        """
        message = "Set fsps and procdevfqdns value"
        self.logger.info(message)
        data = json.loads(argin)
        self.logger.info(f"data: {data}")
        if not all(key in data for key in ("serial", "fqdn")):
            print(f"WARNING: DIDN'T FIND A KEY (serial/fqdn) in {argin}")
            return
        serial_nr = data["serial"][0]
        processor_fqdn = data["fqdn"]
        self._procdevfqdn[serial_nr] = processor_fqdn
        self.push_change_event("procDevFqdn", json.dumps(self._procdevfqdn))

        serial_numbers = data["serial"]
        for serial_nr in serial_numbers:
            if serial_nr == "XFL1RCFEG244":
                if "fsp_01" not in self._fsps:
                    self._fsps["fsp_01"] = []
                if serial_nr not in self._fsps["fsp_01"]:
                    self._fsps["fsp_01"].append(serial_nr)
            elif serial_nr == "XFL1E35JVJTQ":
                if "fsp_02" not in self._fsps:
                    self._fsps["fsp_02"] = []
                if serial_nr not in self._fsps["fsp_02"]:
                    self._fsps["fsp_02"].append(serial_nr)
        self.push_change_event("fsps", json.dumps(self._fsps))

    @command(dtype_out="DevVarLongStringArray")
    @DebugIt()
    def GoToIdle(self):
        self.check_raise_exception()

        def _end_completed():
            self.logger.info("Command GotoIdle completed on device}")
            self.update_fsps()
            self.update_procdevfqdn()

        self.logger.info(f"GoToIdle invoked at time {time.asctime()}")
        result_code, msg = self.do("end", completed=_end_completed)
        return ([result_code], [msg])


# ----------
# Run server
# ----------


def main(args=None, **kwargs):
    """Main function of the device module."""
    return run((LowCbfAllocatorSimulator,), args=args, **kwargs)


if __name__ == "__main__":
    main()
