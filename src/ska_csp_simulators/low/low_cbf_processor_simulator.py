# -*- coding: utf-8 -*-
#
# This file is part of the CSP Simulators project
#
# GPL v3
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

""" Simulator for the processor of a CSP sub-system CBF
"""
from __future__ import annotations

import json
import threading
import time

from ska_control_model import HealthState, ObsState, ResultCode, TaskStatus
from tango import DebugIt, DevState
from tango.server import attribute, command, run

from ska_csp_simulators.common.obs_simulator import ObsSimulatorDevice
from ska_csp_simulators.utilities.data_generator import generate_random_number

__all__ = ["LowCbfProcessorSimulator", "main"]

DevVarLongStringArrayType = tuple[list[ResultCode], list[str]]
# pylint: disable=logging-fstring-interpolation


# set scan time to 5 sec
SCAN_TIME = 5


class RepeatFunction(threading.Timer):
    def run(self):
        while not self.finished.wait(self.interval):
            self.function(*self.args, **self.kwargs)


class LowCbfProcessorSimulator(ObsSimulatorDevice):
    """
    CBF Processor simulator device
    """

    def init_device(self):
        """Initialises the attributes and properties of the device."""
        super().init_device()
        self._health_state = HealthState.OK
        self._stats_mode = '{"ready": "false, "firmware": ""}'
        self._stats_io = {}
        self._subarray_ids = []
        self.timer = RepeatFunction(20, self._update_random_attributes)
        self.set_change_event("stats_mode", True, False)
        self.set_change_event("stats_io", True, False)
        self.set_change_event("subarrayIds", True, False)

    # ---------------
    # General methods
    # ---------------
    def set_communication(
        self: LowCbfProcessorSimulator,
        end_state,
        end_health,
        connecting: bool,
    ):
        """
        Override the behavior of the method to set different
        values fot the State and healthState of the device.
        """

        super().set_communication(DevState.ON, HealthState.UNKNOWN, connecting)

    def _update_random_attributes(self: LowCbfProcessorSimulator):
        if "total_pkts_in" not in self._stats_io:
            self._stats_io["total_pkts_in"] = 0
        if "total_pkts_out" not in self._stats_io:
            self._stats_io["total_pkts_out"] = 0
        if self._obs_state == ObsState.SCANNING:
            self.logger.info(f"Update random attributes at {time.asctime()}")
            self._stats_io["total_pkts_in"] += generate_random_number(
                data_type="int", range_start=0, range_end=1000
            )
            self._stats_io["total_pkts_out"] += generate_random_number(
                data_type="int", range_start=0, range_end=1000
            )
        self.push_change_event("stats_io", json.dumps(self._stats_io))

    # ----------
    # Attributes
    # ----------

    @attribute(
        dtype="DevString",
        label="",
        doc=(""),
    )
    def stats_mode(self: LowCbfProcessorSimulator):
        """Return the statsMode attribute."""
        return str(self._stats_mode)

    @stats_mode.write  # type: ignore[no-redef]
    def stats_mode(self: LowCbfProcessorSimulator, value) -> None:
        self._stats_mode = value

    @attribute(
        dtype="DevString",
        label="Stats IO",
        doc=(""),
    )
    def stats_io(self: LowCbfProcessorSimulator):
        """Return the stats_io attribute."""
        return str(self._stats_io)

    @stats_io.write  # type: ignore[no-redef]
    def stats_io(self: LowCbfProcessorSimulator, value) -> None:
        self._stats_io = value

    @attribute(
        dtype=("DevUShort",),
        max_dim_x=20,
        label="Subarray IDs",
        doc=("Ids of subarrays"),
    )
    def subarrayIds(self: LowCbfProcessorSimulator):
        """Return the subarrayIds attribute."""
        return self._subarray_ids

    @subarrayIds.write  # type: ignore[no-redef]
    def subarrayIds(self: LowCbfProcessorSimulator, value) -> None:
        self._subarray_ids = value

    # --------
    # Commands
    # --------

    @command(dtype_in=str, dtype_out="DevVarLongStringArray")
    @DebugIt()
    def Configure(self: LowCbfProcessorSimulator, argin):
        """
        Processor configure resources
        """
        self.check_raise_exception()

        def _configure_completed():
            self.logger.info("Command Configure completed on device")
            stats_mode_update = {}
            if self._obs_faulty:
                return self.update_obs_state(ObsState.FAULT)
            if self._faulty_in_command:
                self.update_obs_state(ObsState.IDLE)
            if (
                not self._abort_event.is_set()
                and not self._obs_faulty
                and not self._faulty_in_command
            ):
                stats_mode_update["ready"] = self._config_dict["ready"]
                stats_mode_update["firmware"] = self._config_dict["firmware"]
                self._subarray_ids.append(
                    int(self._config_dict["subarray_ids"])
                )
                self._stats_mode = json.dumps(stats_mode_update)
                self.push_change_event("subarrayIds", self._subarray_ids)
                self.push_change_event("stats_mode", self._stats_mode)
                self.update_obs_state(ObsState.READY)
                self.logger.info(f"Configure completed at {time.asctime()}")

        self.logger.info(f"Configure completed at {time.asctime()}")
        self._config_dict = json.loads(argin)
        self.update_obs_state(ObsState.CONFIGURING)
        result_code, msg = self.do(
            "configure",
            completed=_configure_completed,
            argin=self._config_dict,
        )
        return ([result_code], [msg])

    @command(dtype_in=str, dtype_out="DevVarLongStringArray")
    @DebugIt()
    def Scan(self, argin):
        def _scan_completed():
            self.logger.info("Scan completed on device")
            if self._obs_faulty:
                self.update_obs_state(ObsState.FAULT)
            if self._faulty_in_command:
                self.update_obs_state(ObsState.READY)
            self._time_to_complete = 0.4
            self.logger.info(
                f"Reset timeToComplete to {self._time_to_complete} sec"
            )
            self._update_random_attributes()

        self.logger.info(f"Scan invoked at time {time.asctime()}")
        self._time_to_complete = SCAN_TIME
        self.check_raise_exception()
        argin_dict = json.loads(argin)
        self.update_obs_state(ObsState.SCANNING)
        self.timer.start()
        result_code, msg = self.do(
            "scan", completed=_scan_completed, argin=argin_dict
        )
        return ([result_code], [msg])

    @command(dtype_out="DevVarLongStringArray")
    @DebugIt()
    def EndScan(self):
        self.check_raise_exception()

        def _endscan():
            self.logger.info("Event endscan started")
            self._command_tracker.update_command_info(
                command_id, status=TaskStatus.IN_PROGRESS
            )
            time.sleep(0.2)
            msg = "Endscan Task completed successfully"
            self._command_tracker.update_command_info(
                command_id,
                status=TaskStatus.COMPLETED,
                result=[ResultCode.OK, msg],
            )
            self.update_obs_state(ObsState.READY)
            self.timer.cancel()
            self._update_random_attributes()

        self.logger.info(f"EndScan invoked at time {time.asctime()}")
        command_id = self._command_tracker.new_command("endscan")
        self._endscan_event.set()
        threading.Thread(target=_endscan).start()
        result_code, _ = ResultCode.STARTED, "EndScan invoked"
        return ([result_code], [command_id])

    @command(dtype_out="DevVarLongStringArray")
    @DebugIt()
    def GoToIdle(self):
        self.check_raise_exception()

        def _end_completed():
            self.logger.info("Command GotoIdle completed on device")
            # reset stats_mode, stats_io and subarrayIds

        self.logger.info(f"GoToIdle invoked at time {time.asctime()}")
        result_code, msg = self.do("end", completed=_end_completed)
        return ([result_code], [msg])

    @command(dtype_out="DevVarLongStringArray")
    @DebugIt()
    def Abort(self):
        def _abort():
            self._command_tracker.update_command_info(
                command_id,
                status=TaskStatus.IN_PROGRESS,
            )
            time.sleep(0.2)
            msg = "Abort  completed"
            self._command_tracker.update_command_info(
                command_id,
                status=TaskStatus.COMPLETED,
                result=[ResultCode.OK, msg],
            )

        obs_state_ori = self._obs_state

        self.update_obs_state(ObsState.ABORTING)
        command_id = self._command_tracker.new_command("abort")
        self.logger.info("Invoking Abort")

        result_code, _ = ResultCode.STARTED, "Abort invoked"
        if obs_state_ori in [ObsState.IDLE, ObsState.READY]:
            self.update_obs_state(ObsState.ABORTED)
            self._obs_faulty = False
            result_code = ResultCode.OK
        else:
            self.timer.cancel()
            self._update_random_attributes()
            self._abort_event.set()
            threading.Thread(target=_abort).start()
        return ([result_code], [command_id])


# ----------
# Run server
# ----------


def main(args=None, **kwargs):
    """Main function of the device module."""
    return run((LowCbfProcessorSimulator,), args=args, **kwargs)


if __name__ == "__main__":
    main()
