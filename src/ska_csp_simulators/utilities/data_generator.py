import random


def generate_random_number(data_type, range_start, range_end):
    """
    Generate a random number of the specified data type within a given range.

    :param data_type: The data type of the random number ('int' or 'float').
    :param range_start: The start of the range.
    :param range_end: The end of the range.

    :return: A random number of the specified type within the given range.
    """
    if data_type == "int":
        return random.randint(range_start, range_end)
    elif data_type == "float":
        return random.uniform(range_start, range_end)
    else:
        raise ValueError("data_type must be 'int' or 'float'")
